import numpy as np
from os import listdir
from os.path import isfile, isdir
from utils.utils import dir_file_quantity, video_to_frames


desired_shape = (220, 360)
paths_list= ['../dataset/train', '../dataset/test']
for path in paths_list:
    for folder in sorted(listdir(path)):
        folder_path = path + '/' + folder
        if isdir(folder_path):
            for file in listdir(folder_path):
                file_path = folder_path + '/' + file
                if isfile(file_path):
                    file_as_x = video_to_frames(file_path, desired_shape)
