from keras.models import Sequential
from keras.layers import Conv2D, MaxPool2D, Flatten, Dropout, Dense


class FeatureExtractor:

    def __init__(self, window_size, window_steps, input_shape, output_channels=32, activation='relu'):
        self.__cnn = Sequential()
        self.__cnn.add(Conv2D(output_channels,
                              kernel_size=window_size,
                              strides=window_steps,
                              activation=activation,
                              input_shape=input_shape))

    def add_conv_layer(self, window_size, window_steps, output_channels=64, activation='relu'):
        self.__cnn.add(Conv2D(output_channels,
                              kernel_size=window_size,
                              strides=window_steps,
                              activation=activation))

    def add_dense_layer(self, output, activation='relu'):
        self.__cnn.add(Dense(output, activation=activation))

    def add_dropout(self, dropout):
        self.__cnn.add(Dropout(dropout))

    def add_pooling_layer(self, pool_size, window_steps=(1,1)):
        self.__cnn.add(MaxPool2D(pool_size=pool_size, strides=window_steps))

    def add_flattening_layer(self):
        self.__cnn.add(Flatten())

    def get_feature_extractor(self):
        return self.__cnn