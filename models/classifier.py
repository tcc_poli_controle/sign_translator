from keras.models import Sequential, model_from_json
from keras.layers import TimeDistributed, LSTM, Dense, Dropout


class Classifier:
    def __init__(self, feature_extractor, input_shape):
        self.__cnn = feature_extractor
        self.__classifier = self.__create_classifier(input_shape)

    def __create_classifier(self, input_shape):
        clf = Sequential()
        clf.add(TimeDistributed(self.__cnn, input_shape=input_shape))
        return clf

    def add_lstm_layer(self, n_timesteps, dropout, recurrent_dropout, return_sequences=False):
        self.__classifier.add(LSTM(n_timesteps, dropout=dropout, recurrent_dropout=recurrent_dropout, return_sequences=return_sequences))

    def add_hidden_layer(self, nodes, activation):
        self.__classifier.add(Dense(nodes, activation=activation))

    def add_dropout_layer(self, dropout):
        self.__classifier.add(Dropout(dropout))        

    def evaluate_model(self, X_test, y_test, verbose=False):
        scores = self.__classifier.evaluate(X_test, y_test, verbose=verbose)
        return scores

    def get_model(self):
        return self.__classifier

    def load_json_model(self, path):
        with open(path, 'r') as json_file:
            model_json = json_file.read()
        loaded_model = model_from_json(model_json)
        weights_path = path.split('.')[0] + '_weights.h5'
        loaded_model.load_weights(weights_path)
        print('Model loaded from', path)
        return loaded_model

    def print_model_summary(self):
        print(self.__classifier.summary())

    def train(self, X_train, y_train, epochs, batch_size):
        self.__classifier.fit(X_train, y_train, epochs=epochs, batch_size=batch_size)

    def save_json_model(self, path):
        model_json = self.__classifier.to_json()
        with open(path, 'w') as json_file:
            json_file.write(model_json)
        weights_path = path.split('.')[0] + '_weights.h5'
        self.__classifier.save_weights(weights_path)
        print('Saved model to', path)
