import numpy as np
from os import listdir
from os.path import isfile, isdir
from utils.utils import dir_file_quantity, dir_folder_quantity, video_to_input, frames_to_input
from keras.utils import to_categorical

class InputGenerator:

    def generate_input(self, path, n_frames, has_label=True, resize_to=None):
        X = self.__generate_x(path, n_frames, resize_to)
        if has_label:
            y, y_ref = self.__generate_y(path)
            return X, y, y_ref
        return X, None, None

    @staticmethod
    def __generate_y(path):
        y_label = []
        label_dict = {}
        i = 0
        for folder in sorted(listdir(path)):
            if isdir(path + '/' + folder):
                label_dict[folder] = i
                folder_length = dir_folder_quantity(path + '/' + folder)
                for _ in range(folder_length):
                    y_label.append(i)
                i += 1
        y = to_categorical(y_label, num_classes=i)
        return y, label_dict

    @staticmethod
    def __generate_x(path, n_frames, resize_to):
        X = []
        for folder in sorted(listdir(path)):
            folder_path = path + '/' + folder
            if isdir(folder_path):
                for sub_folder in listdir(folder_path):
                    sub_folder_path = folder_path + '/' + sub_folder
                    if isdir(sub_folder_path):
                        file_as_x = frames_to_input(sub_folder_path, n_frames, resize_to)
                        X.append(file_as_x)
        X = np.array(X)
        X = np.reshape(X, X.shape + (1,))
        return X
