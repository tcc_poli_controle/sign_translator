import cv2
import numpy as np
from random import random
from image_processor.image_processor import ImageProcessor
from video_processor.video_processor import VideoProcessor
from utils.utils import video_to_input



video_processor = VideoProcessor('teste_video.mp4')
# video_processor = VideoProcessor(0)
image_processor = ImageProcessor()
# desired_shape = (, )
# im = ImageProcessor().open_image('fundo_teste_3.jpg')
im = video_processor.get_next_frame()
# im = np.rot90(np.rot90(np.rot90(im)))
# im = cv2.resize(im, desired_shape)
# desired_shape = (120, 110)
crop_dimensions = [(100, 80), (700, 380)]
upper_left = crop_dimensions[0]
bottom_right = crop_dimensions[1]
cropped_frame = image_processor.crop_image(im, upper_left, bottom_right)
im2 = ImageProcessor().skin_processing(cropped_frame)
ImageProcessor().show_image(im, 'orig')
ImageProcessor().show_image(cropped_frame, 'cropped')
ImageProcessor().show_image(im2, 'skin')
cv2.waitKey(0)

# input_model = video_to_input('dataset/01-M-01-C.avi', 40)

# desired_shape = (300, 400)
# i = 0
# run_app = True
# while run_app:
#     try:
#         frame = video_processor.get_next_frame()
#         width, height, _ = frame.shape
#         print(frame.shape)
#         upper_left = (round(width*4/10), round(height*1/10))
#         print(upper_left)
#         bottom_right = (round(width*4/10) + desired_shape[0], round(height*1/10) + desired_shape[1])
#         print(bottom_right)
#         cropped_frame = image_processor.crop_image(frame, upper_left, bottom_right)
#         print(cropped_frame.shape)
#         skin = image_processor.skin_processing(cropped_frame)
#         print(skin.shape)
#         image_processor.show_image(cropped_frame, 'a')
#         image_processor.show_image(frame, 'b')
#         cv2.waitKey(0)
#         # image_processor.save_image(cropped_frame, 'results/teste_' + i)
#         i += 1
#         # if random() <= 40/60:
#         #     input_model.append(skin)
#     except:
#         run_app = False

# np_input_model = np.array(input_model)
# print(np_input_model.shape)