import cv2
import numpy as np
from random import random
from os import listdir, mkdir
from os.path import isfile, isdir, exists
from image_processor.image_processor import ImageProcessor
from video_processor.video_processor import VideoProcessor
from utils.utils import video_to_frames


crop_dimensions = [(100, 80), (700, 380)]
path = 'all_dataset/all_videos'
extesion = '.mp4'
for file in listdir(path):
	if isfile(path + '/' + file) and extesion in file:
		video_to_frames(path + '/' + file, crop_dimensions)