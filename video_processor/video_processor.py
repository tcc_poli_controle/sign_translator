import cv2
from image_processor.image_processor import ImageProcessor


class VideoProcessor:

    def __init__(self, video_path):
        self.__video = self.__load_video(video_path)

    def __load_video(self, video_path):
        if type(video_path) == int:
            print('Opening web cam port {0}'.format(video_path))
        else:
            print('Opening video {0}'.format(video_path))
        video = cv2.VideoCapture(video_path)
        return video

    def get_next_frame(self):
        success, frame = self.__video.read()
        if success:
            return frame
        raise Exception('There is not a next frame')

    def get_frames_quantity(self):
        number_of_frames = int(self.__video.get(cv2.CAP_PROP_FRAME_COUNT))
        return number_of_frames

    def get_frame_rate(self):
        frame_rate = int(self.__video.get(cv2.CAP_PROP_FPS))
        return frame_rate

    def release_video(self):
        self.__video.release()
        cv2.destroyAllWindows()
