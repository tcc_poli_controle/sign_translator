import argparse
from models.classifier import Classifier
from models.input_generator import InputGenerator
from models.feature_extractor import FeatureExtractor
from keras import backend as K
import tensorflow as tf


def train(n_frames, epochs, batch_size, drive):
    input_generator = InputGenerator()
    new_size = (120, 110)
    if drive:
        path = '../drive/Colab Notebooks/dataset/'
    else:
        path = 'dataset/'
    X_train, y_train, y_ref = input_generator.generate_input(path + 'train', n_frames=n_frames, resize_to=new_size)
    X_test, y_test, _ = input_generator.generate_input(path + 'test', n_frames=n_frames, resize_to=new_size)
    print(X_train.shape)
    print(X_train.shape[2:])
    # print(y_train)

    y_train = y_train[:,:-1]
    y_test = y_test[:,:-1]
    # print(y_train)

    # Inicia a CNN com 2 camadas de convolucao seguida por um pooling
    feature_extractor = FeatureExtractor((3,3), (1,1), X_train.shape[2:], output_channels=64)
    feature_extractor.add_conv_layer((3,3), (1,1), output_channels=64)
    feature_extractor.add_pooling_layer((3,3), (3,3))
    feature_extractor.add_dropout(0.25)

    # 2 camadas de convolucao seguida por um pooling
    feature_extractor.add_conv_layer((3,3), (1,1), output_channels=128)
    feature_extractor.add_conv_layer((3,3), (1,1), output_channels=128)
    feature_extractor.add_pooling_layer((2,2), (2,2))
    feature_extractor.add_dropout(0.25)

    # 2 camadas de convolucao seguida por um pooling
    feature_extractor.add_conv_layer((3,3), (1,1), output_channels=256)
    feature_extractor.add_conv_layer((3,3), (1,1), output_channels=256)
    feature_extractor.add_pooling_layer((2,2), (2,2))
    feature_extractor.add_dropout(0.25)

    # camada de flattening
    feature_extractor.add_flattening_layer()

    cnn = feature_extractor.get_feature_extractor()
    print(cnn.summary())

    # LSTM
    classifier = Classifier(cnn, X_train.shape[1:], X_train.shape[1], 1, dropout=0.2, recurrent_dropout=0.2)
    cnn_lstm = classifier.get_model()
    classifier.print_model_summary()

    cnn_lstm.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    cnn_lstm.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=epochs, batch_size=batch_size)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('frames', type=int)
    parser.add_argument('epochs', type=int)
    parser.add_argument('batch_size', type=int)
    parser.add_argument('--gpu', dest='gpu', action='store_true')
    parser.add_argument('--drive', dest='drive', action='store_true')

    args = parser.parse_args()
    frames = args.frames
    epochs = args.epochs
    batch_size = args.batch_size
    gpu = args.gpu

    if gpu:
        print('using GPU')
        with K.tf.device('/gpu:1'):
            config = tf.ConfigProto(intra_op_parallelism_threads=4,
                                    inter_op_parallelism_threads=4,
                                    allow_soft_placement=True,
                                    device_count={'CPU': 1, 'GPU': 1})
            config.gpu_options.allow_growth = True
            config.gpu_options.per_process_gpu_memory_fraction = 0.4
            session = tf.Session(config=config)
            K.set_session(session)
            print('[LOG] Starting training')
            train(frames, epochs, batch_size, args.drive)
    else:
        train(frames, epochs, batch_size, args.drive)
