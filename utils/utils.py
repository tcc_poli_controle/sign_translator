import numpy as np
from os import listdir, mkdir
from os.path import isfile, isdir, exists
from random import random
from fractions import Fraction
from video_processor.video_processor import VideoProcessor
from image_processor.image_processor import ImageProcessor


def dir_file_quantity(path):
    length = 0
    for file in listdir(path):
        if isfile(path + '/' + file):
            length += 1
    return length

def dir_folder_quantity(path):
    length = 0
    for folder in listdir(path):
        if isdir(path + '/' + folder):
            length += 1
    return length

def video_to_input(path, n_frames, desired_shape, processor='skin'):
    video_as_input = []
    video_processor = VideoProcessor(path)
    image_processor = ImageProcessor()
    total_frames = video_processor.get_frames_quantity()
    desire_ration = Fraction(n_frames, total_frames)
    i = 0
    while i < total_frames:
        for _ in range(desire_ration.numerator):
            frame = video_processor.get_next_frame()
            width, height, _ = frame.shape
            upper_left = (int(width * 4 / 10), int(height * 1 / 10))
            bottom_right = (int(width * 4 / 10) + desired_shape[0], int(height * 1 / 10) + desired_shape[0])
            cropped_frame = image_processor.crop_image(frame, upper_left, bottom_right)
            if processor == 'skin':
                processed_img = image_processor.skin_processing(cropped_frame)
            else:
                processed_img = image_processor.binary_processing(cropped_frame)
            video_as_input.append(processed_img)
        i += desire_ration.denominator
    return video_as_input

def video_to_frames(path, crop_dimensions, processor='skin'):
    video_processor = VideoProcessor(path)
    image_processor = ImageProcessor()
    total_frames = video_processor.get_frames_quantity()
    path_splited = path.split('/')
    frames_path = path_splited[:-1]
    frames_path.append(path_splited[-1].split('.')[0])
    frames_path = '/'.join(frames_path)
    if not exists(frames_path):
        mkdir(frames_path)
    frames_path = frames_path + '/frames'
    if not exists(frames_path):
        mkdir(frames_path)
    for i in range(total_frames):
        current_frames_path = frames_path + '/' + str(i) + '.png'
        frame = video_processor.get_next_frame()
        width, height, _ = frame.shape
        upper_left = crop_dimensions[0]
        bottom_right = crop_dimensions[1]
        cropped_frame = image_processor.crop_image(frame, upper_left, bottom_right)
        if processor == 'skin':
            processed_img = image_processor.skin_processing(cropped_frame)
        else:
            processed_img = image_processor.binary_processing(cropped_frame)

        image_processor.save_image(processed_img, current_frames_path)

def frames_to_input(path, n_frames, resize_to):
    model_input = []
    total_frames = dir_file_quantity(path + '/frames')
    all_frames = range(total_frames)
    selected_frames = np.random.choice(all_frames, n_frames, replace=False)
    selected_frames.sort()
    for i in selected_frames:
        frame_path = path + '/frames/' + str(i) + '.png'
        frame = ImageProcessor().open_image(frame_path, flag=0)
        if resize_to:
            frame = ImageProcessor().resize_image(frame, resize_to)
        model_input.append(frame)
    return model_input
