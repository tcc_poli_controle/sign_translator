import cv2
import numpy as np


class ImageProcessor:

    min_value = 70
    low_range = np.array([0, 50, 80])
    upper_range = np.array([30, 200, 255])
    skinkernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))

    @classmethod
    def binary_processing(cls, img):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        blur = cv2.GaussianBlur(gray, (5, 5), 2)
        threshold = cv2.adaptiveThreshold(blur,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,11,2)
        _, res = cv2.threshold(threshold, cls.min_value, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
        return res

    @classmethod
    def skin_processing(cls, img):
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

        mask = cv2.inRange(hsv, cls.low_range, cls.upper_range)

        mask = cv2.erode(mask, cls.skinkernel, iterations=1)
        mask = cv2.dilate(mask, cls.skinkernel, iterations=1)

        mask = cv2.GaussianBlur(mask, (15, 15), 1)
        res = cv2.bitwise_and(img, img, mask=mask)
        res = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)

        return res

    @staticmethod
    def save_image(img, path):
        print('Saving {0} img'.format(path))
        cv2.imwrite(path, img)

    @staticmethod
    def add_rectangle(img, upper_left, bottom_right, bgr_color, thickness):
        new_img = cv2.rectangle(img, upper_left, bottom_right, bgr_color, thickness)
        return new_img

    @staticmethod
    def crop_image(img, upper_left, bottom_right):
        cropped_img = img[upper_left[1]:bottom_right[1], upper_left[0]:bottom_right[0]]
        return cropped_img

    @staticmethod
    def copy_image(img):
        copy = np.copy(img)
        return copy

    @staticmethod
    def show_image(img, window_name):
        return cv2.imshow(window_name, img)

    @staticmethod
    def open_image(img, flag=1):
        return cv2.imread(img, flag)

    @staticmethod
    def resize_image(img, new_size):
        return cv2.resize(img, new_size)